from django.contrib import admin
from recipes.models import Recipe, RecipeStep, Ingredient

@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "id",
        "rating",
        "created_on",
    ]

@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display =(
        "recipe_title",
        "instruction",
        "order",
        "id",
)

@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display =(

        "amount",
        "food_item",
        "id",
    )