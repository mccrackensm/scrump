from django.forms import ModelForm
from recipes.models import Recipe, RecipeStep
# from django import forms

# class RecipeForm(forms.ModelForm):
#     email_address = forms.EmailField(max_length=300)

class RecipeForm(ModelForm):

    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
            "rating",
        ]

class RecipeStepForm(ModelForm):

    class Meta:
        model = RecipeStep
        fields = [
        "instruction",
        "order",
        ]
    