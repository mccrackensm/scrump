from django.urls import path
from . import views

urlpatterns = [
    path("", views.recipe_list, name = "recipe_list"),
    path("<int:id>", views.show_recipe, name = "show_recipe"),
    path("create/", views.create_recipe, name = "create_recipe"),
    path("<int:id>/edit/", views.edit_recipe, name = "edit_recipe"),
    path("author/", views.my_recipe_list, name = "my_recipe_list"),

]
